import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class MainKtTest {
    val testInput = "3,4,3,1,2"

    @Test
    fun calculateAmountFishesAfter26DaysExampleTest() {
        assertEquals(26, calculateAmountFishesAfterXDays(testInput, 18))
    }

    @Test
    fun calculateAmountFishesAfter80DaysExampleTest() {
        assertEquals(5934, calculateAmountFishesAfterXDays(testInput, 80))
    }

    @Test
    fun calculateAmountFishesAfter256DaysExampleTest() {
        assertEquals(26984457539, calculateAmountFishesAfterXDays(testInput, 256))
    }
}