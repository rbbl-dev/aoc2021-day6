import java.io.File

const val maxReproductionTime = 8
const val adultReproductionTime = 6

fun main() {
    val input = File("input.txt").readText().trim()
    println(calculateAmountFishesAfterXDays(input, 256))
}

fun calculateAmountFishesAfterXDays(input: String, days: Int): Long {
    var statusFishCountMap = inputToFishMap(input)
    for (day in 0..days) {
        val result = hashMapOf<Int, Long>()
        for (i in maxReproductionTime downTo 1) {
            result[i - 1] = statusFishCountMap[i] ?: 0
        }
        result[adultReproductionTime] = (result[6] ?: 0) + (statusFishCountMap[0] ?: 0)
        result[maxReproductionTime] = statusFishCountMap[0] ?: 0
        statusFishCountMap = result
    }
    return statusFishCountMap.filter { it.key != 8 }.map { it.value }.sum()
}

fun inputToFishMap(input: String): HashMap<Int, Long> {
    val fishMap = hashMapOf<Int, Long>()
    input.split(",").map { it.toInt() }.forEach {
        val value = fishMap[it]
        fishMap[it] = (value ?: 0) + 1
    }
    return fishMap
}